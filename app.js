require('./config/config');
const showRouter = require('./routes/shows.routes');
const stateRouter = require('./routes/state.routes');

const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT;
// const showcontrol = require(`./controller/showsControl`)

var app = express();

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
// app.use(function (err, req, res, next) {
    
//     res.status(500).send('Something went wrong!!');
// });

// app.use('/api/:state',(request ,response,next)=>{
//     console.log("hellow");
//     next();
// })
//app.post(`/api/:state`,showcontrol.createShow)
// //app.use('/api/:state', stateRouter);
// app.param('state',(request,response,next,val)=>{
//     response.locals.state=val;
//     console.log(response.locals.state);
//     next();
// })
app.use('/api/shows', showRouter);
app.use('/api',stateRouter);



app.get('/', (request, response) => {
    response.send("movie ticket management");
})



app.listen(port, () => console.log(`Server started at port : ${port}`))
