const { Client } = require('pg');
const client = new Client({
    connectionString: process.env.connectionString
});
client.connect()
    .then(() => {
        console.log("database  connected  successfully");
    })
    .catch(() => {
        console.log("error in database connectivity");
    })

module.exports = {
    createShow: (request, response) => {
        const state = request.params.state;
        const schema = `CREATE SCHEMA IF NOT EXISTS ${state};`
        client.query(schema, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "error in creation of state schema"
                })
            }
            else {
                const table = `CREATE TABLE ${state}.show(id serial PRIMARY KEY, moviename text,timings time,day date,city varchar(20),fare numeric);`
                client.query(table, (error, result) => {
                    if (error) {
                        console.log(error);
                        response.status(500).json({
                            message: "error in creation of show"
                        })
                    }
                    else {
                        response.status(201).json({
                            message: "show table created sucessfully created"
                        })
                    }
                })
            }

        })
    },

    insertShow: (request, response) => {
        state = request.params.state;
        const { moviename, timings, day, city, fare } = request.body;
        const insertShow = `INSERT INTO ${state}.show (moviename,timings,day,city,fare) VALUES('${moviename}','${timings}','${day}','${city}',${fare})`;
        client.query(insertShow, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "There was a problem to add a show details"
                })

            }
            else {
                const getId = `SELECT id from ${state}.show ORDER BY id DESC LIMIT 1`;
                client.query(getId, (error, results) => {
                    id = results.rows[0].id;
                    response.location(`/shows/${state}/${id}`);
                    response.status(201).json({
                        message: "show added sucessfully"
                    })
                })
            }
        })
    },

    displayShow: (request, response) => {
        const state = request.params.state;
        const id = request.params.id;
        client.query(`SELECT * from ${state}.show where id=${id}`, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "error happened while get the details of the show"
                })

               //next(error);
            }
            else {

                response.status(200).json(results.rows);
            }
        })
    },
    displayShows: (request, response) => {
        const state = request.params.state;

        client.query(`SELECT * from ${state}.show ORDER BY id ASC`, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "error happened while get the list of the show"
                })
            }
            else {
                response.status(200).json(results.rows);
            }
        })
    },
    deleteShow: (request, response) => {
        const id = request.params.id;
        const state = request.params.state;


        client.query(`DELETE FROM ${state}.show WHERE id = ${id}`, (error, results) => {

            if (error) {
                response.status(500).json({
                    message: "error happened while deletion of the show"
                })
            }
            else {
                response.status(200).json({
                    message: "show deleted sucessfully"
                });
            }
        })
    },
    updateShow: (request, response) => {
        const id = request.params.id;
        const state = request.params.state;
        const { moviename, timings, day, city, fare } = request.body;
        client.query(`UPDATE ${state}.show SET moviename = '${moviename}',
        timings='${timings}',day='${day}',city='${city}',fare=${fare} where id=${id}`,
            (error, results) => {
                if (error) {
                    response.status(500).json({
                        message: "There was a problem in update a show"
                    });
                }
                else {

                    response.status(200).json({
                        message: "Show updated sucessfully"
                    })
                    console.log(results.rows);
                }
            }
        )
    }
    // errorLog:(error,request,response)=>{
    //     response.status(500).json({
    //         message:"error happened"
    //     })
    // }

}


