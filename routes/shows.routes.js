const express = require('express');
const router = express.Router();
const showsControl = require('../controller/showsControl');

router.post('/:state', showsControl.insertShow);
router.get('/:state', showsControl.displayShows);
router.get('/:state/:id', showsControl.displayShow);
router.put('/:state/:id', showsControl.updateShow);
router.delete('/:state/:id', showsControl.deleteShow);

module.exports = router; 
