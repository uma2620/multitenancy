const express = require('express');
const router = express.Router();
const showsControl =require('../controller/showsControl');

router.post('/:state',showsControl.createShow);

module.exports=router;